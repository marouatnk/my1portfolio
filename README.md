# My first Portfolio

## Creation of a portfolio for simplonlyon.fr.

***

### User Stories :


As a recruiter I want to get access to a portfolio that showcases the preferences and the skills of the person in question.

As a Colleague I want to see other portfolios in order to get inspiration from them .

As a professor I want to see the progress of my students and also see the level of upgradation in their programming / soft  skills.

### Mockup : 

| <img src="img/landingpage.png" alt="landingpage" title="acceuil" width="250" height="370" /> | <img src="img/menubar.png" alt="menu bar" title="menu" width="250" height="370" />|
	
| <img src="img/project.png" alt="work" title="projects" width="250" height="370" /> | <img src="img/aboutme.png" alt="about me" title="Who Am I ?" width="250" height="370" />|
	
 <img src="img/contact.png" alt="contact "	title="Contact me!" width="250" height="370" />


The languages I used and its versions: 

+ Bootstrap 4 
+ HTML 5
+ CSS
+ JavaScript
+ GSAP

## Contributors

 Tounekti Maroua <maroua.tnk@gmail.com>

## Licence & Copyright

 @ Tounekti Maroua | Simplon lyon .

